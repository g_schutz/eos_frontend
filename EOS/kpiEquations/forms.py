from django.forms import ModelForm
from EOS.data_db.models import KpiEquations

class KpiEqForm(ModelForm):
    class Meta:
        model = KpiEquations

    def __init__(self, *args, **kwargs):
        super(KpiEqForm, self).__init__(*args, **kwargs)
        for fi in ['kpi_name','component_list','equation','constraints']:
            self.fields[fi].widget.attrs['cols'] = 80
            self.fields[fi].widget.attrs['rows'] = 1
        for fi in ['free_text','issues']:
            self.fields[fi].widget.attrs['cols'] = 80
            self.fields[fi].widget.attrs['rows'] = 3
