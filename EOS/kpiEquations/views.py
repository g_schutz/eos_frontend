from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.forms.models import modelformset_factory

from forms import KpiEqForm
from EOS.data_db.models import KpiEquations as KpiEq

def NewKpiEquation(request):
    if request.method == 'POST':
        form = KpiEqForm(request.POST)
        if form.is_valid():
            return HttpResponseRedirect('')
    else:
        form = KpiEqForm()

    return render_to_response('NewKpiEquation.html', 
                  {'form': form,})
    
def KpiEquations(request):
    KpiEqFormSet = modelformset_factory(KpiEq,form=KpiEqForm)
    if request.method == 'POST':
        formset = KpiEqFormSet(request.POST)
        if formset.is_valid():
            return HttpResponseRedirect('')
    else:
        formset = KpiEqFormSet()

    return render_to_response('KpiEquations.html', 
                  {'formset': formset,})
