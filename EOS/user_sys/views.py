from django.shortcuts import render
#from django.contrib.auth import authenticate, login, logout
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from registration_defaults.settings import *
from django.contrib.auth.models import User
from EOS.request import *
from EOS.user_sys.models import *
from django.core.cache import cache
from EOS.user_sys.forms import *

@login_required()
def organisation_page(request):    
    """
    Create a page where an user can see all the plant he has access to # and
    Change selected plant form
    """
    userName = request.user
    name = get_organisation_name(userName)    
    is_orga_manager = check_organisation_manager(userName);
    is_name_valid = False
    is_user_authorized = False
    #define access
    for org in organisation.objects.all():
        if str(org.organisation_name) == name:
            is_name_valid = True
            break
    if is_name_valid == True:
        obj = organisation.objects.get(organisation_name=name)
        for memb in obj.organisation_member.all():
            if str(memb.username) == str(userName):
                is_user_authorized = True
                break
    plantList = getPlant(userName)
    if is_orga_manager: # if the user is manager, extend the list of his plant with all the plant of the organisation
        plantList += get_plant_list(name)
        plantList = list(set(plantList))
    nbRequest = getNbRequest(is_orga_manager, userName)    
    selected = getSelectedPlant(userName)
    alSelected = checkIfSelectedPlant(selected)
    if request.method == "POST":
        form = changeSelectedForm(request.POST)        
        if form.is_valid():
            #this form allows to an user to change his selected plant
            plant_name = form.cleaned_data['name']
            try:
                plant = plants.objects.get(plants_name = plant_name)
            except plants.DoesNotExist:
                plant = None
            if plant == None or \
               plant.plantKey not in plantList:
                #if plant is not in organisation, dot nothing
                return HttpResponseRedirect('')
            try:
                Myobj = selectedPlant.objects.get(P_user__username=userName)
                Myobj.delete()
            except :
                pass
            nvSelection = selectedPlant(P_user=User.objects.get(username=userName), 
                                        P_plant=plants.objects.get(plantKey=plant.plantKey))
            nvSelection.save()
            return HttpResponseRedirect('')
    else:
        form = changeSelectedForm()
    inOrga = True
    if name == "None":
        inOrga = False
    Plants = plants.objects.filter(plantKey__in = plantList)
    return render(request, 'user_sys/organisation_page.html', 
                  {'inOrga': inOrga,'selected': selected, 'form': form,
                   'alSelected': alSelected, 'is_orga_manager':is_orga_manager,
                   'organiName': name, 'is_name_valid': is_name_valid, 
                   'is_user_authorized': is_user_authorized, 
                   'plantList': Plants,
                   'nbRequest': getNbRequest(is_orga_manager, userName)})

@login_required()
def join_organisation(request):
    """
    send request to join an organisation.
    """
    userName = request.user
    if request.method == "POST":
        form = joinOrganisationForm(request.POST)
        if form.is_valid():
            mail = form.cleaned_data['organisationEmail']
            managerSel = getUserNameByEmail(mail)
            if managerSel == None: #if error
                return render(request, 'user_sys/join_organisation.html', {'form': form, 'error': "Error: can't find : %s" % mail})

            #Check if a request is already send
            reqst = None
            try:
                reqst = requestManager.objects.get(ruser__username=userName, rtype="Join organisation", rmanager__username=managerSel)                
            except:
                reqst = None
            if reqst != None:                
                return render(request, 'user_sys/join_organisation.html', {'form': form, 'error': "Error: you already requested to join this organisation."})
            orga = get_organisation_name(managerSel)
            nrequest = requestManager(rtype="Join organisation", rmanager=managerSel, ruser=userName)
            nrequest.save()
            return render(request, 'user_sys/join_organisation_confirm.html', {'orga': orga})
    else:
        form = joinOrganisationForm()
    return render(request, 'user_sys/join_organisation.html', {'form': form, 'error': ""})


@login_required()
def ask_plant_right(request):
    """
    send a request, to have access to a plant, to the manager of the user's organisation
    """

    userName = request.user
    pltList = []
    nvPlantList = []
    for orga in getOrganisationList(userName):
        pltList += get_plant_list(orga) #extend the list with all the plant of the organisation
    alreadyAccessPlant = getPlant(userName)
    for plt in pltList:
        if plt not in alreadyAccessPlant:
            nvPlantList.append(plt)
    if request.method == "POST":
        form = askPlantRightForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            reqst = None
            try:
                reqst = requestManager.objects.get(ruser__username=userName, 
                                                   rtype="Access plant", 
                                                   rmessage="Access to %s" % name ,
                                                   rmanager__username=managerSel)       
            except:
                reqst = None
            if reqst != None:
                return render(request, 'user_sys/ask_plant_right.html', 
                              {'form': form, 'error': "Error: you already have to access to this plant."})

            managerSel = organisation.objects.get(plants_list__plants_name=name).organisation_manager
            nrequest = requestManager(rtype="Access plant", rmanager=managerSel, ruser=userName, rmessage="Access to %s" % name)
            nrequest.save()
            return render(request, 'homepage/index.html')
    else:
        form = askPlantRightForm()
    Plants = plants.objects.filter(plantKey__in = nvPlantList)
    return render(request, 'user_sys/ask_plant_right.html', 
                  {'form': form, 'plantList': Plants, "error": ""})

@login_required()
def request_page(request):
    """
    The page where the manager can check the request
    Currently 2 types of requests : join an organisation and ask for access to a plant.
    """
    managerName = request.user
    requestList = getRequestList(managerName)
    selected = getSelectedPlant(managerName)
    alSelected = checkIfSelectedPlant(selected)
    if request.method == "POST":
        form = requestPageForm(request.POST)
        if form.is_valid():
            myval = form.cleaned_data['val']
            mytyp = form.cleaned_data['typ']
            mymess = form.cleaned_data['mess']
            myuser = form.cleaned_data['use']
            if myval == "Decline":
                requestManager.objects.filter(ruser__username=myuser, rtype=mytyp, rmessage=mymess).delete()
                return HttpResponseRedirect('') #(add success mess ?)
            #if the manager press 'approve'
            if mytyp == "Join organisation": #(add condition if error)
                usr = User.objects.get(username__exact=myuser)
                obj = organisation.objects.get(organisation_manager__username=managerName)
                obj.organisation_member.add(usr)
                requestManager.objects.filter(ruser__username=myuser, rtype=mytyp, rmessage=mymess).delete()                
                return HttpResponseRedirect('') #(add success mess)
            elif mytyp == "Access plant":
                plantName = mymess.split(" ")[-1]
                usr = User.objects.get(username__exact=myuser)
                obj = plants.objects.filter(plants_name=plantName)
                obj[0].plants_member.add(usr)                
                requestManager.objects.filter(ruser__username=myuser, rtype=mytyp, rmessage=mymess).delete()
                return HttpResponseRedirect('')
    else:
        form = requestPageForm()

    return render(request, 'user_sys/request_page.html', {'form': form, 'requestList': requestList, 'selected': selected,
        'alSelected': alSelected})
