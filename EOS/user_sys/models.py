from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User, Group
from django.db.models.signals import post_save

class plants(models.Model):
    """
    Create the 'plants' object
    """
    plants_name = models.CharField(max_length=50) #name of the plant
    plantKey = models.CharField(max_length=5,unique=True) #Key or accronym of the plant
    plants_member = models.ManyToManyField(User, blank=True) #list of member = relation with the default User object
    def __unicode__(self):
        return u'%s' % self.plants_name
    class Meta:
        verbose_name = ('Plants')

class plantsAdmin(admin.ModelAdmin):
    list_display = ["plants_name"]
    search_fields = ["plants_name"]

admin.site.register(plants, plantsAdmin)

class organisation(models.Model):
    """
    Create the 'organisation' object
    """

    organisation_name = models.CharField(max_length=50) #name of the organisation
    organisation_member = models.ManyToManyField(User) #list of member = relation with the default User object
    plants_list = models.ManyToManyField(plants) #list of plant = relation with the plants object
    organisation_manager = models.ForeignKey(User, related_name='+') #list of manager = relation with the default User object

    def __unicode__(self):
        return u'%s' % self.organisation_name
    class Meta:
        verbose_name = ('Organisation')

class organisationAdmin(admin.ModelAdmin):
    list_display = ["organisation_name"]
    search_fields = ["organisation_name"]

admin.site.register(organisation, organisationAdmin)

class requestManager(models.Model):
    """
    Create the 'requestManager' object
    Used to save the request sent to a manager
    """

    rtype = models.CharField(max_length=50)
    rmessage = models.TextField(blank=True)
    rmanager = models.ForeignKey(User)
    ruser = models.ForeignKey(User, related_name='+')

    def __unicode__(self):
        return u'%s' % self.rtype
    class Meta:
        verbose_name = ('Request')

class requestManagerAdmin(admin.ModelAdmin):
    list_display = ["rtype", "rmanager", "ruser"]
    search_fields = ["rtype"]

admin.site.register(requestManager, requestManagerAdmin)

class selectedPlant(models.Model):
    """
    Create the 'selectedPlant' Object
    Used to save the selected plant of an user
    """
    P_user = models.ForeignKey(User)
    P_plant = models.ForeignKey(plants, related_name='+')
    def __unicode__(self):
        return u'%s' % self.P_plant
    class Meta:
        verbose_name = ('selectedPlant')

class selectedPlantAdmin(admin.ModelAdmin):
    list_display = ["P_user", "P_plant"]
    search_fields = ["P_plant"]

admin.site.register(selectedPlant, selectedPlantAdmin)
