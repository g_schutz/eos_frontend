from django import forms

class createOrganisation(forms.Form):
    Name = forms.CharField(max_length=50)

class joinOrganisationForm(forms.Form):
    """
    Form used in user_sys/view.py in join_organisation()
    """
    organisationEmail = forms.EmailField()

class requestPageForm(forms.Form):
    """
    Form used in user_sys/view.py in request_page()
    """
    val = forms.CharField(max_length=30)
    typ = forms.CharField(max_length=40)
    mess = forms.CharField(max_length=400, required=False)
    use = forms.CharField(max_length=50)

class changeSelectedForm(forms.Form):
    """
    Form used in user_sys/view.py in organisation_page()
    """
    name = forms.CharField(max_length=50)

class askPlantRightForm(forms.Form):
    """
    Form used in user_sys/view.py in ask_plant_right()
    """
    name = forms.CharField(max_length=50)
