# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):

    def forwards(self, orm):
        "Write your forwards methods here."
        nameKeyDict = {'Burg':"BUR",'Heiderscheider_Grund':"HSG"}
        for pi in orm.plants.objects.all():
            if pi.plants_name in nameKeyDict:
                pi.plantKey = nameKeyDict[pi.plants_name]
            else:
                pi.plantKey = 'ERROR'
                #later this will produce an error as plantKey will be an unique attribute.
            pi.save()

    def backwards(self, orm):
        "Write your backwards methods here."
        # No backwards needed as the plantKey will be removed.

    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'user_sys.organisation': {
            'Meta': {'object_name': 'organisation'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'organisation_manager': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': "orm['auth.User']"}),
            'organisation_member': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.User']", 'symmetrical': 'False'}),
            'organisation_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'plants_list': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['user_sys.plants']", 'symmetrical': 'False'})
        },
        'user_sys.plants': {
            'Meta': {'object_name': 'plants'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'plantKey': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'plants_member': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.User']", 'symmetrical': 'False', 'blank': 'True'}),
            'plants_name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'user_sys.requestmanager': {
            'Meta': {'object_name': 'requestManager'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'rmanager': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'rmessage': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'rtype': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'ruser': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': "orm['auth.User']"})
        },
        'user_sys.selectedplant': {
            'Meta': {'object_name': 'selectedPlant'},
            'P_plant': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': "orm['user_sys.plants']"}),
            'P_user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['user_sys']
    symmetrical = True
