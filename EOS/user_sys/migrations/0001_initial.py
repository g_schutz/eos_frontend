# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'plants'
        db.create_table('user_sys_plants', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('plants_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('user_sys', ['plants'])

        # Adding M2M table for field plants_member on 'plants'
        m2m_table_name = db.shorten_name('user_sys_plants_plants_member')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('plants', models.ForeignKey(orm['user_sys.plants'], null=False)),
            ('user', models.ForeignKey(orm['auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['plants_id', 'user_id'])

        # Adding model 'organisation'
        db.create_table('user_sys_organisation', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('organisation_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('organisation_manager', self.gf('django.db.models.fields.related.ForeignKey')(related_name='+', to=orm['auth.User'])),
        ))
        db.send_create_signal('user_sys', ['organisation'])

        # Adding M2M table for field organisation_member on 'organisation'
        m2m_table_name = db.shorten_name('user_sys_organisation_organisation_member')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('organisation', models.ForeignKey(orm['user_sys.organisation'], null=False)),
            ('user', models.ForeignKey(orm['auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['organisation_id', 'user_id'])

        # Adding M2M table for field plants_list on 'organisation'
        m2m_table_name = db.shorten_name('user_sys_organisation_plants_list')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('organisation', models.ForeignKey(orm['user_sys.organisation'], null=False)),
            ('plants', models.ForeignKey(orm['user_sys.plants'], null=False))
        ))
        db.create_unique(m2m_table_name, ['organisation_id', 'plants_id'])

        # Adding model 'requestManager'
        db.create_table('user_sys_requestmanager', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('rtype', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('rmessage', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('rmanager', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('ruser', self.gf('django.db.models.fields.related.ForeignKey')(related_name='+', to=orm['auth.User'])),
        ))
        db.send_create_signal('user_sys', ['requestManager'])

        # Adding model 'selectedPlant'
        db.create_table('user_sys_selectedplant', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('P_user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('P_plant', self.gf('django.db.models.fields.related.ForeignKey')(related_name='+', to=orm['user_sys.plants'])),
        ))
        db.send_create_signal('user_sys', ['selectedPlant'])


    def backwards(self, orm):
        # Deleting model 'plants'
        db.delete_table('user_sys_plants')

        # Removing M2M table for field plants_member on 'plants'
        db.delete_table(db.shorten_name('user_sys_plants_plants_member'))

        # Deleting model 'organisation'
        db.delete_table('user_sys_organisation')

        # Removing M2M table for field organisation_member on 'organisation'
        db.delete_table(db.shorten_name('user_sys_organisation_organisation_member'))

        # Removing M2M table for field plants_list on 'organisation'
        db.delete_table(db.shorten_name('user_sys_organisation_plants_list'))

        # Deleting model 'requestManager'
        db.delete_table('user_sys_requestmanager')

        # Deleting model 'selectedPlant'
        db.delete_table('user_sys_selectedplant')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'user_sys.organisation': {
            'Meta': {'object_name': 'organisation'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'organisation_manager': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': "orm['auth.User']"}),
            'organisation_member': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.User']", 'symmetrical': 'False'}),
            'organisation_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'plants_list': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['user_sys.plants']", 'symmetrical': 'False'})
        },
        'user_sys.plants': {
            'Meta': {'object_name': 'plants'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'plants_member': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.User']", 'symmetrical': 'False', 'blank': 'True'}),
            'plants_name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'user_sys.requestmanager': {
            'Meta': {'object_name': 'requestManager'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'rmanager': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'rmessage': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'rtype': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'ruser': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': "orm['auth.User']"})
        },
        'user_sys.selectedplant': {
            'Meta': {'object_name': 'selectedPlant'},
            'P_plant': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': "orm['user_sys.plants']"}),
            'P_user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['user_sys']