from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth.views import login, logout
admin.autodiscover()

urlpatterns = patterns('EOS.homepage.views',
    url(r'^$', 'index', name='homepage_index'),
    url(r'^home/$', 'index', name='homepage_index'),
    url(r'^settings/$', 'settings', name='homepage_settings'),
    url(r'^manageUser/$', 'manageUser', name='homepage_manageUser'),
    url(r'^createPlant/$', 'createPlant', name='homepage_createPlant'),
    url(r'^addUserRight/$', 'addUserRight', name='homepage_addUserRight'),
    url(r'^Myfeedback/$', 'Myfeedback', name='homepage_feedback'),    
    url(r'^benchmark/$', 'benchmark', name='homepage_benchmark'),    
    url(r'^support/$', 'support', name='homepage_support'),    
    url(r'^plt/$', 'plantPage', name='homepage_plant'),
    url(r'^plt/(\w*)/$', 'plantPage', name='homepage_plant'),
    url(r'^plt/(\w*)/(\w*)/$', 'plantPage', name='homepage_plant'),
)

#urlpatterns += patterns('EOS.kpiEquations.views',
#    url(r'^kpiEqs/$', 'KpiEquations'),
#    url(r'^newKpiEq/$', 'NewKpiEquation'),
#)

urlpatterns += patterns('EOS.user_sys.views',
    url(r'^organisationpg/$', 'organisation_page', name='user_organisation_page'),
    url(r'^joinorga/$', 'join_organisation', name='user_join_organisation'),
    url(r'^askplant/$', 'ask_plant_right', name='user_ask_plant_right'),
    url(r'^request_page/$', 'request_page', name='user_request_page'),    
)

urlpatterns += patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('registration.backends.default.urls')),
)
