# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#     * Rearrange models' order
#     * Make sure each model has one field with primary_key=True
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.

from django.db import models
from django.contrib import admin
from django.db import IntegrityError
from django.db.models.base import ModelBase
from django.forms import ModelForm, ValidationError
from django.contrib import messages
import re

class KpiParts(models.Model):
    date = models.DateTimeField(null=False)
    sensor_id = models.TextField(blank=False)
    value = models.FloatField(null=True)
    interval_id = models.IntegerField(null=True)
    interval_type = models.TextField(null=True)
    plant = models.TextField(null=False)

    class Meta:
        db_table = u'kpi_parts'
        managed = False

class Kpi(models.Model):
    date = models.DateTimeField(null=False)
    kpi_id = models.TextField(blank=True)
    value = models.FloatField(null=True)
    interval_id = models.IntegerField(null=True)
    interval_type = models.TextField(null=True)
    plant = models.TextField(null=False)

    class Meta:
        db_table = u'kpis'
        managed = False

class KpiAdmin(admin.ModelAdmin):
    list_display = ["date", "kpi_id", "value", "interval_id", "plant"]
    search_fields = ["kpi_id", "interval_type",]
    list_filter = ['plant', ]

admin.site.register(Kpi, KpiAdmin)

class KpiEquations(models.Model):
    kpi_name = models.TextField()
    component_list = models.TextField()
    equation = models.TextField()
    constraints = models.TextField(blank=True,null=True)
    related_benchmark = models.IntegerField(blank=True)
    is_kpi = models.BooleanField()
    free_text = models.TextField()
    issues = models.TextField(blank=True)

    class Meta:
        db_table = u'kpi_equations'
        managed = False

class KPIEquationsAdminForm(ModelForm):
    class Meta:
        model = KpiEquations

    def __init__(self, *args, **kwargs):
        super(KPIEquationsAdminForm, self).__init__(*args, **kwargs)
        ## Change the wigit cols/rows attributes for all text fields
        for fi in ['kpi_name','component_list','equation','constraints']:
            self.fields[fi].widget.attrs['cols'] = 80
            self.fields[fi].widget.attrs['rows'] = 1
        for fi in ['free_text','issues']:
            self.fields[fi].widget.attrs['cols'] = 80
            self.fields[fi].widget.attrs['rows'] = 3

    #== first all field specific cleans are specified
    #====
    def clean_kpi_name(self):
        kpiStr = self.cleaned_data["kpi_name"].strip()
        # do something that validates your data
        if not re.match('^(KPI|KPIp)_\w+', kpiStr):
            raise ValidationError("The kpi name seems not to have a correct structure (KPI[p]_xxx!")
        return kpiStr

    def clean_component_list(self):
        clistStr = self.cleaned_data["component_list"].strip()
        # First clean a common error with trailing ','
        if clistStr.endswith(',)'):
            clistStr = clistStr[:-2]+')'
            self.data["component_list"] = clistStr # This makes the change visible in the updated form.
        # do something that validates your data
        if not re.match('^[ \'\(\),\w]+$',clistStr):
            raise ValidationError("There are unsupportat characters in the field!")
        if not re.match('^\((\'\w+\'[ ,]*)+\)$',clistStr):
            raise ValidationError("The overall structure of the component_list is not valide!"+\
                                  " -> ('C1','C2',...)")
        return clistStr

    def clean_equation(self):
        # do something that validates your data
        eqStr = self.cleaned_data["equation"].strip()
        eqItems = re.findall('item(\d{1,})\.\w+',eqStr)
        if len(eqItems) < 1:
            raise ValidationError("There are no items defined in this equation (invalid item specification?)!")
        itemIndexs = list(set(int(xi) for xi in eqItems))
        if itemIndexs[0] != 1:
            raise ValidationError("The item indexing must start at index=1!")
        if itemIndexs[-1] != len(itemIndexs):
            raise ValidationError("The item indexing must be continious!")
        return eqStr

    def clean_constraints(self):
        constStr = self.cleaned_data["constraints"].strip()
        # do something that validates your data
        if len(constStr) > 0 and len(re.findall('item\d{1,}\.\w+',constStr)) < 1:
            raise ValidationError("Constraints are malformed, there are no items used!")
        if constStr == '':
            constStr = None
        return constStr

    #== here now the global form clean and validation is done
    #====
    def clean(self):
        super(KPIEquationsAdminForm, self).clean()
        # do overall data validations here

        # Check for coherence on is_kpi and kpi_name
        if self.cleaned_data.get("kpi_name") == None or \
           self.cleaned_data.get("is_kpi") == None :
            #Nothing to validate here as the fields as not yet cleaned.
            pass
        elif self.cleaned_data.get("is_kpi") == True and \
           not self.cleaned_data.get("kpi_name",'').startswith('KPI_'):
            raise ValidationError("Entry is flagged as 'kpi' but the name does not start with 'KPI_'!")
        elif self.cleaned_data.get("is_kpi") == False and \
             not self.cleaned_data.get("kpi_name",'').startswith('KPIp_'):
            raise ValidationError("Entry is flagged as 'no-kpi' but the name does not start with 'KPIp_'!")

        # Check for item coherence in component_list, equation and constraints
        if not all(k in self.cleaned_data for k in ("component_list","equation","constraints")):
            #this means that on of those was not validated on a previous validation level
            pass
        else: # All necessary fields for global validation are cleaned.
            # Get the component list items
            CompList = map(lambda x:x.strip("' "),
                           self.cleaned_data["component_list"].strip(" '()").split(','))
            # Get the equation items
            eqItems = set([xi.split('.')[0] for xi in \
                           re.findall('item\d{1,}\.\w+',
                                      self.cleaned_data["equation"])])
            if len(CompList) < len(eqItems):
                raise ValidationError("There more items used in equation (%d) than defined in the component list (%d)!" % (len(eqItems),len(CompList),))
            if len(CompList) > len(eqItems):
                raise ValidationError("There are more items specifyed in the component list (%d) than used in equation (%d)!" % (len(CompList),len(eqItems),))
            # Get the constraints items
            if self.cleaned_data["constraints"]:
                x = re.findall('item\d{1,}\.\w+',self.cleaned_data["constraints"])
                consItems = set([xi.split('.')[0] for xi in x])
            else:
                consItems = set()
            if not consItems <= eqItems:
                raise ValidationError("There are constriants on items not specifyed in the equation (%s)!" % (consItems-eqItems,))

        return self.cleaned_data


class KpiEquationsAdmin(admin.ModelAdmin):
    list_display = ["kpi_name", "free_text",]
    search_fields = ["kpi_name", "component_list",]
    list_filter = ['is_kpi', ]
    form = KPIEquationsAdminForm

    def save_model(self, request, obj, form, change):
        try:
            obj.save()
        except IntegrityError, error:
            # do sum checking of the error and possible cleaned_data removals
            # check how to interact here with the form for error message.
            messages.error(request,"There was an DB IntegrityError: %s" % (error,))

admin.site.register(KpiEquations,KpiEquationsAdmin)
