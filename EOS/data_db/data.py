def dictfetchall(cursor):
        "Returns all rows from a cursor as a dict"
        desc = cursor.description
        cols = [col[0] for col in desc]
        return [ dict(zip(cols, row))
        for row in cursor.fetchall()
    ]

from datetime import datetime
typehandler = lambda obj: "string" if obj.type_code == 1114 else "number"
formathandler = lambda obj: obj.isoformat() if isinstance(obj, datetime) else obj

def to_json_fetchall(cursor):
        "Returns all rows from a cursor as a dict to be used for build json"
        d = { "cols": [], "rows": [], }
        desc = cursor.description
        for col in desc:
            d['cols'].append({"id":"","label":col[0],"pattern":"","type":typehandler(col)})
        for row in cursor.fetchall():
            dcol = {"c":[],}
            for i in xrange(len(desc)):
                ri = formathandler(row[i])
                dcol['c'].append({"v":ri,"f":None})
            d['rows'].append(dcol)
        return d
