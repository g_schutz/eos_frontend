class data_defRouter(object):
    """
    Router for the second DB.
    """
    def db_for_read(self, model, **hints):
        if model._meta.app_label == 'data_db':
            return 'data_def'
        return 'default'

    def db_for_write(self, model, **hints):
        if model._meta.app_label == 'data_db':
            return 'data_def'
        return 'default'
    
    def allow_relation(self, obj1, obj2, **hints):
        if obj1._meta.app_label == 'data_db' and obj2._meta.app_label == 'data_db':
            return True
        elif 'data_db' not in [obj1._meta.app_label, obj2._meta.app_label]: 
            return True
        return False
    
    def allow_syncdb(self, db, model):
        if db == 'data_def' or model._meta.app_label == "data_db":
            return False
        else:
            return True
