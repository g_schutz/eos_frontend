var data;

function drawOrgChartAndTable() {
    var data = google.visualization.arrayToDataTable([
        ['Name',  'Manager'],
        ['WWTP Global',  null],
        ['Mechanical treatment step',  null],
        ['Biological treatment step',  null],
        ['Sludge treatment step',  null],
        ['Further treatment steps',  null],
        ['Energy related KPI',  null],
        ['Process related KPI', null]
    ]);
    
    var orgchart = new google.visualization.OrgChart(document.getElementById('orgchart'));
    orgchart.draw(data, {size: "medium"});
    
    google.visualization.events.addListener(orgchart, 'select', function() {
        var item = orgchart.getSelection()[0];
        var sect = data.getFormattedValue(item.row, 0).replace(/ /gi, "_");
        var url = window.location.href;
	url = url.slice(0,url.search("/plt/")+5).concat(EOSplantKey,'/',sect,'/')
        window.location.href = url;
    });

    //ToDo use a js variable for the selected part, find item in data and set selection here.
    orgchart.setSelection([{row:0,column:null}]);
}
google.setOnLoadCallback(drawOrgChartAndTable);

