google.load('visualization', '1.1', {packages: ['corechart', 'controls']});

function sludge_2() {
    var data = google.visualization.arrayToDataTable([
						      ['T°', 'sludge age'],
						      [1, 10],
						      [2, 12],
						      [3, 14],
						      [4, 16],
						      [5, 20],
						      [6, 22],
						      [7, 26],
						      [8, 30],
						      [9, 35],
						      [10, 35],
						      ]);

    new google.visualization.LineChart(document.getElementById('sludge_2')).
        draw(data, {curveType: "function",
                    focusTarget: 'category',
                    pointSize: 5,
                    colors: ['red'],
                    width: 1200, height: 600,
                    title: 'Sludge age and Temperature per day',
                    vAxis: {title: "Sludge Age", minValue: 0},
                    hAxis: {title: "Temperature in C°", minValue: 0}}
            );
}
google.setOnLoadCallback(sludge_2);