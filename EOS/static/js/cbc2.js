google.load("visualization", "1", {packages:["corechart"]});
google.load('visualization', '1.0', {'packages':['controls']});

function combochart_2() {
    var data = google.visualization.arrayToDataTable([
						      ['day', 'blowers1', 'blowers2', 'blowers3'],
						      ['2013-01-01',  13360, 38176, 97406],
						      ['2013-01-02',  14381, 39683, 92887],
						      ['2013-01-03',  14765, 40632, 10634],
						      ['2013-01-04',  15790, 50690, 10690],
						      ['2013-01-05',  15800, 40800, 10800],
						      ['2013-01-06',  16800, 30800, 10800],
						      ['2013-01-07',  17800, 20800, 10800],
						      ]);

    // Create and draw the visualization.                                                  
    new google.visualization.ColumnChart(document.getElementById('cbc_2')).
        draw(data,
             {title:"Blowers consumption for each blower",
		     width: 1200, height: 600,
		     hAxis: {title: "Year"},
		     vAxis: {title: "Kwh"}}
	     );
}
google.setOnLoadCallback(combochart_2);
