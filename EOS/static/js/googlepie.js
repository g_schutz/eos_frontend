function drawVisualization() {
    var data = google.visualization.arrayToDataTable([
        ['Energy consumption', '%'],
        ['Mechanical treatment', 4.4365361803],
        ['Biology', 59.5729537367],
        ['sludge treatment', 17.3902728351],
        ['Advanced treatment', 8.3274021352],
        ['Infrastructure', 10.2728351127]
    ]);
    var option = {
    }
    new google.visualization.PieChart(document.getElementById('visualization_pie')).
        draw(data, {title:"Energy consumption"}, option);
}
google.setOnLoadCallback(drawVisualization);