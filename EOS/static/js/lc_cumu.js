google.load('visualization', '1', {packages: ['controls']});
google.load('visualization', '1.1', {packages: ['corechart', 'controls']});

function lc_cumu() {
    var data = google.visualization.arrayToDataTable([
						      ['x', 'Kpi cumulative'],
						      ['d1', 0],
						      ['d2', 5],
						      ['d3', 15],
						      ['d4', 20],
						      ['d5', 40],
						      ['d6', 45],
						      ['d7', 60],
						      ['d8', 60],
						      ['d9', 75],
						      ['d10', 90],
						      ['d11', 130],
						      ['d12', 130],
						      ['d13', 135],
						      ]);

    new google.visualization.LineChart(document.getElementById('lc_cumu')).
        draw(data, {curveType: "function",
                    colors: ['lime'],
                    width: 1200, height: 600,
                    hAxis: {title: "Time"},
                    vAxis: {title: "Kpi"}}
            );
}
google.setOnLoadCallback(lc_cumu);