google.load("visualization", "1", {packages:["corechart"]});
google.load('visualization', '1', {packages:['table']});
google.load('visualization', '1', {packages:['orgchart']});
google.load('visualization', '1.0', {'packages':['controls']});
google.load('visualization', '1', {'packages':['annotatedtimeline']});
google.load('visualization', '1', {packages: ['controls']});
google.load('visualization', '1.0', {packages: ['charteditor']});
google.load('visualization', '1.1', {packages: ['corechart', 'controls']});
google.load('visualization', '1', {packages:['table']});

function drawVisualization() {
    var data = google.visualization.arrayToDataTable([
						      ['Name', 'Date S', 'Date E','Energy comsumption'],
						      ['WWTP Global', '2013-01-01', '2013-01-02',15],
						      ['Mechanical treatement step', '2013-01-02', '2013-01-03',15],
						      ['Biological treatemant step', '2013-01-03', '2013-01-04',15],
						      ['Sludge treatement step', '2013-01-04', '2013-01-05',15],
						      ['Further treatement step', '2013-01-05', '2013-01-06',15],
						      ['Emergy related Kpi', '2013-01-06', '2013-01-07',15],
						      ['Process Related Kpi', '2013-01-07', '2013-01-08',15]
						      ]);

    var slider = new google.visualization.ControlWrapper({
	    'controlType': 'StringFilter',
	    'containerId': 'control1',
	    'options': {
		'filterColumnLabel': 'Date S',
		'ui': {'labelStacking': 'vertical'}
	    }
	});
    var slider_2 = new google.visualization.ControlWrapper({
	    'controlType': 'StringFilter',
	    'containerId': 'control1bis',
	    'options': {
		'filterColumnLabel': 'Date E',
		'ui': {'labelStacking': 'vertical'}
	    }
	});
    var categoryPicker = new google.visualization.ControlWrapper({
	    'controlType': 'CategoryFilter',
	    'containerId': 'control2',
	    'options': {
		'filterColumnLabel': 'Name',
		'ui': {
		    'allowTyping': false,
		    'allowMultiple': true,
		    'selectedValuesLayout': 'belowStacked',
		}
	    }
	});
    var pie = new google.visualization.ChartWrapper({
	    'chartType': 'PieChart',
	    'containerId': 'chart1',
	    'options': {
		'width': 400,
		'height': 400,
		'legend': 'none',
		'title': 'energy in %',
		'chartArea': {'left': 15, 'top': 15, 'right': 0, 'bottom': 0},
		'pieSliceText': 'label'
	    },
	    'view': {'columns': [0, 3]}
	});

    var table = new google.visualization.ChartWrapper({
	    'chartType': 'Table',
	    'containerId': 'chart2',
	    'options': {
		'width': 400,
		'height': 400,
	    }
	});

    new google.visualization.Dashboard(document.getElementById('dashboard')).
        bind([slider, slider_2, categoryPicker], [pie, table]).
        draw(data);
}
google.setOnLoadCallback(drawVisualization);