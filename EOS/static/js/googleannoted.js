google.load('visualization', '1', {packages: ['annotatedtimeline']});

function drawVisualization() {
    var data = new google.visualization.DataTable();
    data.addColumn('date', 'Date');
    data.addColumn('number', 'Mechanical Treatment');
    data.addColumn('number', 'Biology');
    data.addColumn('number', 'Sludge treatment');
    data.addColumn('number', 'Advanced treatment');
    data.addColumn('number', 'Infrastructure');
        data.addRows([
              [new Date(2012, 1 ,1), 187, 2500, 733, 350, 433],
              [new Date(2012, 1 ,2), 190, 2550, 734, 355, 433],
              [new Date(2012, 1 ,3), 191, 2580, 735, 360, 435],
              [new Date(2012, 1 ,4), 192, 2600, 736, 375, 434],
              [new Date(2012, 1 ,5), 193, 2580, 740, 380, 432],
              [new Date(2012, 1 ,6), 194, 2620, 741, 385, 431],
              [new Date(2012, 1 ,7), 193, 2680, 740, 375, 433],
              [new Date(2012, 1 ,8), 190, 2580, 738, 360, 436],
              [new Date(2012, 1 ,9), 187, 2520, 738, 350, 433],
              [new Date(2012, 1 ,10), 187, 2522, 739, 350, 437],
              [new Date(2012, 1 ,11), 186, 2510, 736, 345, 436],
              [new Date(2012, 1 ,12), 185, 2500, 735, 344, 432],
              [new Date(2012, 1 ,13), 187, 2460, 734, 348, 430],
              [new Date(2012, 1 ,14), 186, 2420, 735, 347, 434],
              [new Date(2012, 1 ,15), 187, 2480, 736, 350, 433],
              [new Date(2012, 1 ,16), 185, 2475, 735, 351, 431],
              [new Date(2012, 1 ,17), 186, 2550, 733, 352, 432],
              [new Date(2012, 1 ,18), 184, 2570, 732, 355, 431],
              [new Date(2012, 1 ,19), 182, 2530, 730, 355, 433],
              [new Date(2012, 1 ,20), 181, 2500, 733, 358, 435],
              [new Date(2012, 1 ,21), 184, 2420, 736, 360, 438],
              [new Date(2012, 1 ,22), 186, 2430, 735, 358, 440],
              [new Date(2012, 1 ,23), 187, 2480, 731, 355, 437],
              [new Date(2012, 1 ,24), 187, 2499, 734, 354, 434],
              [new Date(2012, 1 ,25), 188, 2509, 733, 350, 433],
              [new Date(2012, 1 ,26),  188, 2530, 731, 349, 432],
              [new Date(2012, 1 ,27), 187, 2540, 730, 350, 430],
              [new Date(2012, 1 ,28), 187, 2511, 733, 351, 433]
              ]);

    var annotatedtimeline = new google.visualization.AnnotatedTimeLine(
                                       document.getElementById('vizualisation_annoted'));
    annotatedtimeline.draw(data, {'displayAnnotations': false, 'allValuesSuffix': 'kWh/d', 'displayExactValues': true});
}

google.setOnLoadCallback(drawVisualization);
