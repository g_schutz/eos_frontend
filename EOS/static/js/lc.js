google.load('visualization', '1.1', {packages: ['corechart', 'controls']});

function lc() {
    var data = google.visualization.arrayToDataTable([
                                                      ['day', 'sluge age'],
                                                      ['d1', 10],
                                                      ['d2', 12],
                                                      ['d3', 14],
                                                      ['d4', 16],
                                                      ['d5', 20],
                                                      ['d6', 22],
                                                      ['d7', 26],
                                                      ['d8', 30],
                                                      ['d9', 35],
                                                      ['d10', 35],
                                                      ]);

    // Create and draw the visualization.                                                
    new google.visualization.LineChart(document.getElementById('sludge')).
        draw(data, {curveType: "function",
                    pointSize: 5,
                    focusTarget: 'category',
                    colors: ['red'],
                    width: 1200, height: 600,
                    title: 'Sludge age and Temperature per day',
                    vAxis: {title: "Sludge Age", minValue: 0},
                    hAxis: {title: "Days", minValue: 0}}
            );
}
google.setOnLoadCallback(lc);
