google.load("visualization", "1", {packages:["corechart"]});
google.load('visualization', '1.0', {'packages':['controls']});

function combochart() {
    var data = google.visualization.arrayToDataTable([
						      ['Blowers', '2013-01-01', '2013-01-02', '2013-01-03', '2013-01-04', '2013-01-05', '2013-01-06', '2013-01-07'],
						      ['blower 1',  13360, 38176, 97406, 11047, 66518, 15720, 12000],
						      ['blower 2',  15381, 39683, 92887, 11519, 59401, 17350, 12000],
						      ['blower 3',  15765, 40632, 10634, 11564, 57140, 16710, 12000],
						      ]);

    // Create and draw the visualization.                                                  
    new google.visualization.ColumnChart(document.getElementById('cbc')).
        draw(data,
             {title:"Blowers consumption for each blower",
		     width: 1200, height: 600,
		     hAxis: {title: "Year"},
		     vAxis: {title: "Kwh"}}
	     );
}
google.setOnLoadCallback(combochart);