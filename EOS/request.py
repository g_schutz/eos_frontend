#from django.contrib.auth import authenticate, login, logout                    
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from registration_defaults.settings import *
from EOS.user_sys.models import *
from django.contrib.auth.models import User

def check_organisation_manager(userName):
    """Return true if user is organisation manager """
    is_orga_manager = False
    try: #check si l'user fait parti du group "organisation manager" rechercher le nom
        Manager_organiName = organisation.objects.filter(organisation_manager=userName)[0]
        is_orga_manager = True
    except :
        Manager_organiName = "None"
        is_orga_manager = False
    return is_orga_manager

def get_organisation_name(userName):
    """Get the name of the user's organisation"""
    organisationName = "None"
    for org in organisation.objects.all():
        for memb in org.organisation_member.all():
            if memb == userName:
                organisationName = org
    return organisationName

def get_plant_list(orgaName):
    """Get the list of all the plants of an organisation"""
    plants_list = []
    obj = organisation.objects.filter(organisation_name=orgaName)
    for e in obj:
        for f in e.plants_list.all():
            plants_list.append(f.plantKey)
    return plants_list

def get_orga_user_list(userName, organisationName):
    """Get list of user (without managers) of an organisation"""
    mbr_list = []
    for obj in organisation.objects.filter(organisation_name=organisationName):
        for memb in obj.organisation_member.all():
            if check_organisation_manager(memb) == False: #check si check_organisation_manager(memb) == False
                mbr_list.append(memb)
    return mbr_list

def getOrgaManagerList(organisationName):
    """recupere la liste des managers d'une organisation"""
    mbr_list = []
    for obj in organisation.objects.filter(organisation_name=organisationName):
        for memb in obj.organisation_member.all():
            if check_organisation_manager(memb) == True: #check si check_organisation_manager(memb) == False
                mbr_list.append(memb)
    return mbr_list

def getUserNameByEmail(Email):
    """Search username"""
    try:
        return User.objects.get(email=Email)    
    except:
        return None

def getRequestList(managerName):
    """ Return list of request for the organisation manager"""
    requestList = []
    obj = requestManager.objects.filter(rmanager=managerName)
    for e in obj:
        requestList.append([e.rtype, e.ruser, e.rmessage])
    return requestList

def getSelectedPlant(userName):
    """Return selected plant"""
    try:
        obj = selectedPlant.objects.get(P_user__username=userName)        
    except :
        return None    
    return obj.P_plant

def checkIfSelectedPlant(selected):
    """return true if the user has selected a plant"""
    if selected == None:
        selected = ""
        alSelected = False
    else:
        alSelected = True
    return alSelected

def getNbRequest(is_orga_manager, userName):
    """Return the number of request"""
    nbRequest = 0
    if is_orga_manager == True:
        nbRequest = len(getRequestList(userName))
    return nbRequest

def getPlant(user):
    """
    Get list of plant for a given user
    - user should be a name (string) of an existing user or an instance of User.
    The method returns a list of plant keys refering to the plants a user has access to.
    """
    Plants = plants.objects.filter(plants_member__username = user)
    plants_list = [pi.plantKey for pi in Plants]
    return plants_list

def getPlantOrganisation(user, orgaName):
    """
    Get all the plants of a member of an organisation
    """
    orgs = organisation.objects.filter(organisation_name=orgaName)
    Plants = plants.objects.filter(plants_member__username = user)
    Plants = Plants.filter(organisation__in=orgs)
    plants_list = [pi.plantKey for pi in Plants]
    return plant_list

def getOrganisationList(userName):
    """
    Get the organisation name of an user.
    """    
    organisationList = []
    for org in organisation.objects.all():
        if userName in org.organisation_member.all():
            organisationList.append(str(org))
    return organisationList
