from django.shortcuts import render, render_to_response
from django.utils import simplejson
from django.contrib.auth.models import User
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import Max
from django.db import connections

from EOS.user_sys.models import *
from EOS.request import *
from EOS.homepage.forms import *
from EOS.data_db.models import *
from EOS import settings as EOSSettings

#API that allow to create graphics
import gviz_api

from datetime import datetime

from registration_defaults.settings import *

@login_required()
def index(request):
    """
    View for the homepage
    """
    userName = request.user
    is_orga_manager = check_organisation_manager(userName)
    organisationName = get_organisation_name(userName)
    nbRequest = getNbRequest(is_orga_manager, userName)    
    selected = getSelectedPlant(userName)
    alSelected = checkIfSelectedPlant(selected)
    plants_list = get_plant_list(organisationName)
    dic = {'plants_list': plants_list,
           'organiName': organisationName,
           'is_orga_manager': is_orga_manager,
           'nbRequest': nbRequest,
           'selected': selected,
           'alSelected': alSelected,
           }
    return render(request, 'homepage/index.html', dic)

@login_required()
def manageUser(request):
    """
    view for the manage user part
    In this page we can just remove an user to a plant
    """

    userName = request.user
    is_user_authorized = check_organisation_manager(userName)
    Rlist = []
    organisationName = get_organisation_name(userName)
    MembreList = get_orga_user_list(userName, organisationName)
    PlantList = get_plant_list(organisationName)
    managerList = getOrgaManagerList (organisationName)
    for memb in MembreList:
        Rlist.append([memb, getPlantOrganisation(memb, organisationName)])

    if request.method == "POST":
        supForm = SupprUserRForm(request.POST)
        if supForm.is_valid():
            #Get info from the form and delete the user from the chosen plant
            name = supForm.cleaned_data['name']
            plantName = supForm.cleaned_data['plantName']            
            UserInPlant = getPlantOrganisation(name, organisationName)
            if plantName in UserInPlant:
                usr = User.objects.get(username__exact=name) #get user object   
                obj = plants.objects.filter(plants_name=plantName) #get plant object
                #Delete user in userlist of a plant / remove the relation
                obj[0].plants_member.remove(usr)
            return HttpResponseRedirect('')
    else:
        supForm = SupprUserRForm()
    return render_to_response('homepage/manageUser.html', {'is_user_authorized': is_user_authorized, 'managerList': managerList, 'MembreList': MembreList, 'PlantList': PlantList, 'supprForm': supForm, 'list': Rlist}, context_instance=RequestContext(request))

@login_required()
def addUserRight(request):
    """
    Same as manageUser but here we add an user to a plant.
    """

    userName = request.user
    organisationName = get_organisation_name(userName)
    MembreList = get_orga_user_list(userName, organisationName)
    PlantList = get_plant_list(organisationName)

    if request.method == 'POST':
        addForm = AddUserForm(request.POST)
        if addForm.is_valid():
            #Get info from the form and add the user in the plant.
            Aname = addForm.cleaned_data['Aname']
            AplantName = addForm.cleaned_data['AplantName']
            #Check the request
            UserInPlant = getPlant(Aname)            
            if AplantName in UserInPlant:
                #ToDo: add error mess to be reported
                pass
            else:
                usr = User.objects.get(username__exact=Aname)                
                obj = plants.objects.filter(plants_name=AplantName)
                obj[0].plants_member.add(usr)
            return HttpResponseRedirect('/manageUser/')
    else:
        addForm = AddUserForm()
    return render_to_response('homepage/addUserRight.html', {'MembreList': MembreList, 'PlantList': PlantList, 'addForm': addForm}, context_instance=RequestContext(request))

@login_required()
def Myfeedback(request):
    """
    Provisional funct
    Made to collect feedback
    """
    if request.method == "POST":
        contact_form = ContactForm(request.POST)
        if contact_form.is_valid():
            success = True
            email = contact_form.cleaned_data['email']
            information = contact_form.cleaned_data['information']
            problem = contact_form.cleaned_data['problem']
            send_mail("EOS feedback",
                      "From : %s\n===================\n%s\n===================\n%s" % (email, information, problem), 
                      EOSSettings.MANAGERS[0][1],
                      ["georges.schutz@tudor.lu"]) #Currently the emails are send to my address. Need to be changed to an admin.
    else:
        contact_form = ContactForm()
    ctx = {'contact_form': contact_form}
    return render_to_response('homepage/Myfeedback.html', ctx, context_instance=RequestContext(request))

@login_required()
def createPlant(request):
    """
    Allows to create a new plant.
    """
    userName = request.user
    is_user_authorized = check_organisation_manager(userName)
    organisationName = get_organisation_name(userName)
    if request.method == "POST":
        nvPlant = AddNewPlant(request.POST)
        if nvPlant.is_valid():
            name = nvPlant.cleaned_data['p_name']            #Get the name of the new plant by the form
            obj = organisation.objects.filter(organisation_name=organisationName) #Get the object of the organisation of the connected user (manager)
            nPlant = plants(plants_name=name) #Create a new plant (object)
            nPlant.save()
            obj[0].plants_list.add(nPlant) #Link the new plant created to the selected organisation
            return HttpResponseRedirect('/home/')
    else:
        nvPlant = AddNewPlant()
    return render_to_response('homepage/createPlant.html', {'nvPlant':nvPlant, 'is_user_authorized': is_user_authorized}, context_instance=RequestContext(request))

@login_required()
def plantPage(request, plantNameReq=None, sectionSel='WWTP_Global'):
    """
    View of the plant page
    """

    #get some info on the connected user
    userName = request.user
    is_orga_manager = check_organisation_manager(userName)
    organisationName = get_organisation_name(userName)
    nbRequest = getNbRequest(is_orga_manager, userName)    
    selected = getSelectedPlant(userName)
    alSelected = checkIfSelectedPlant(selected)
    plantList = getPlant(userName)

    if is_orga_manager:
        plantList += get_plant_list(organisationName)
        plantList = list(set(plantList))

    if plantNameReq == None:
        plantNameURLReq = selected
    else:
        #Check the access right of the requested
        try:
            plantNameURLReq = plants.objects.get(plantKey = plantNameReq) #ToDo if url request is swiched to plantKey
        except plants.DoesNotExist:
            plantNameURLReq = None
    if plantNameURLReq == None or plantNameURLReq.plantKey not in plantList:
        #return an error page if the user has no access to the requested page
        return render(request, 'homepage/errorAccess.html',
                      { 'ReqPlantRef': plantNameReq, 
                        'selected': selected,} ) 

    ### Handle the Graph Parameter Form data
    if request.method == 'POST':
        PGPform = PlantGraphParams(request.POST)
        if PGPform.is_valid():
            request.session['PlantGraphParams'] = PGPform.cleaned_data
        else:
            pass #GScToDo: handle error in the posted data.
    elif request.session.get('PlantGraphParams',None):
        PGPform = PlantGraphParams(request.session['PlantGraphParams'])
    else:
        PGPinit = {'SDate':'2013-01-01', #ToDo use some thing usefull here
                   'EDate':'2013-11-01', #ToDo use latest data available for the plant (Week type)
                   'IntervalType':'week'}
        PGPform = PlantGraphParams(PGPinit)
        PGPValid = PGPform.is_valid()
        request.session['PlantGraphParams'] = PGPform.cleaned_data
    PGP = request.session['PlantGraphParams']

    ### Build the data selection depending on request
    lastupdate = None
    plantRef = plantNameURLReq.plantKey
    PlantKPi = Kpi.objects.filter(plant=plantRef)
    PlantParts = KpiParts.objects.filter(plant=plantRef)
    lastupdate = PlantKPi.filter(interval_type=PGP['IntervalType']).aggregate(Max('date'))['date__max']
    ### Get the KPIs to be represented
    kpiID = PlantKPi.distinct("kpi_id")
    if sectionSel == 'WWTP_Global':
        newKPi = kpiID.filter(kpi_id__regex="^KPI_WW(TP)*_")
    elif sectionSel == 'Mechanical_treatment_step':
        newKPi = kpiID.filter(kpi_id__regex=".*(SAND)")
    elif sectionSel == 'Biological_treatment_step':
        newKPi = kpiID.filter(kpi_id__regex=".*BIO")
    elif sectionSel == 'Further_treatment_steps':
        newKPi = None
    elif sectionSel == 'Energy_related_KPI':
        newKPi = kpiID.filter(kpi_id__regex=".*(ELW|ENERGY)")
    else:
        newKPi = None
    ### If any kpi, build the SQL request
    if newKPi:
        kpi = zip(*newKPi.values_list('kpi_id'))[0]
        #Build the crosstab query to get the data for the selected KPIs
        whereKPI = ' or '.join(["kpi_id = ''%s''"%ki for ki in kpi])
        asKPI = ', '.join(["%s float"%ki for ki in kpi])
        descL = [(ki,'number') for ki in kpi]
        sql = """SELECT * 
        FROM crosstab(
          'select date, kpi_id, value 
           from kpis 
           where (%(KPIWhere)s) AND interval_type = ''%(TimeRes)s''
             AND date BETWEEN ''%(SDate)s'' AND ''%(EDate)s''
           order by 1,2') 
    AS kpi(date timestamp, %(KPIas)s) 
    ORDER BY date DESC 
    LIMIT 500;""" % {'KPIWhere':whereKPI,'KPIas':asKPI,
                     'TimeRes':PGP['IntervalType'],
                     'SDate':PGP['SDate'],'EDate':PGP['EDate']}
    else:
        sql = None

    if sql:
        conn = connections['data_def']
        c = conn.cursor()
        c.execute(sql)
        d = c.fetchall()
    else:
        d = None


    #json conversion
    if d:
        descL.insert(0,('Date','datetime',))
        data_table = gviz_api.DataTable(descL)
        data_table.LoadData(d)

        jsonCode = data_table.ToJSCode("jscode_data",
           #columns_order=("Date", "BUR_WWTP_CONS_CAL_ELWq_2H", "BUR_WWTP_WWTP_CAL_COVel_2H", "BUR_COGE_PROD_CAL_ELW_2H"),
           order_by="Date")
    else:
        jsonCode = None

    return render_to_response('homepage/plantPage.html', 
                              {'lastupdate': lastupdate,
                               'PGPForm':PGPform,
                               'jTs': jsonCode, 
                               'nbRequest': nbRequest,
                               'is_orga_manager': is_orga_manager, 
                               'plant': plantNameURLReq,
                               'selected': selected,
                               'alSelected': alSelected,
                               'sectionSel':sectionSel},
                              context_instance=RequestContext(request))


@login_required()
def settings(request):
    userName = request.user
    is_orga_manager = check_organisation_manager(userName)
    organisationName = get_organisation_name(userName) 
    nbRequest = getNbRequest(is_orga_manager, userName)    
    selected = getSelectedPlant(userName)
    alSelected = checkIfSelectedPlant(selected)
    return render_to_response('homepage/settings.html', {'nbRequest': nbRequest, 'is_orga_manager': is_orga_manager, 'organisationName': organisationName, 'selected': selected,
            'alSelected': alSelected}, context_instance=RequestContext(request))

@login_required()
def benchmark(request):
    userName = request.user
    is_orga_manager = check_organisation_manager(userName)
    organisationName = get_organisation_name(userName) 
    nbRequest = getNbRequest(is_orga_manager, userName)    
    selected = getSelectedPlant(userName)
    alSelected = checkIfSelectedPlant(selected)
    return render_to_response('homepage/benchmark.html', {'nbRequest': nbRequest, 'is_orga_manager': is_orga_manager, 'organisationName': organisationName, 'selected': selected,
        'alSelected': alSelected}, context_instance=RequestContext(request))

@login_required()
def support(request):
    userName = request.user
    is_orga_manager = check_organisation_manager(userName)
    organisationName = get_organisation_name(userName) 
    nbRequest = getNbRequest(is_orga_manager, userName)    
    selected = getSelectedPlant(userName)
    alSelected = checkIfSelectedPlant(selected)
    return render_to_response('homepage/support.html', {'nbRequest': nbRequest, 'is_orga_manager': is_orga_manager, 'organisationName': organisationName, 'selected': selected,
        'alSelected': alSelected}, context_instance=RequestContext(request))


