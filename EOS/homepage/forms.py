from django import forms

class ContactForm(forms.Form):
    """Form used in homepage/view.py in Myfeedback()"""
    email = forms.EmailField()
    information = forms.CharField(widget=forms.Textarea)
    problem = forms.CharField(widget=forms.Textarea)

class SupprUserRForm(forms.Form):
    """ """
    name = forms.CharField(max_length=100)
    plantName = forms.CharField(max_length=100)

class AddUserForm(forms.Form):
    """Form used in homepage/view.py in addUserRight()"""
    Aname = forms.CharField(max_length=100)
    AplantName = forms.CharField(max_length=100)

class AddNewPlant(forms.Form):
    """Form used in homepage/view.py in createPlant()"""
    p_name = forms.CharField(max_length=100, label="Plant name")

class PlantGraphParams(forms.Form):
    """Form used to specify the parameters for the Pland data visualisation"""
    SDate = forms.DateField(label="Start-date",
                            help_text="First day of the analized date range")
    EDate = forms.DateField(label="End-date",
                            help_text="Last day of the analized date range")
    IntervalType = forms.ChoiceField(label="Interval type",
                                     help_text="Select the resolution of the analized data",
                                     choices=[('day','Day'),
                                              ('week','Week'),
                                              ('month','Month')],
                                     initial='week')
